FROM debian:stable

RUN apt -y update && apt -y upgrade 
RUN apt -y install  curl \
                    libcurl4-openssl-dev \
                    libxml2-dev \
                    libssl-dev \
                    libfontconfig1-dev \
                    libharfbuzz-dev \
                    libfribidi-dev \
                    libfreetype6-dev \
                    libpng-dev \
                    libtiff5-dev \
                    libjpeg-dev \
                    pandoc \
                    pandoc-citeproc \
                    pandoc-data \
                    pandoc-sidenote

# Install latest R
RUN apt -y install dirmngr gnupg apt-transport-https ca-certificates software-properties-common && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-key '95C0FAF38DB3CCAD0C080A7BDC78B2DDEABC47B7' && \
    add-apt-repository 'deb http://cloud.r-project.org/bin/linux/debian bullseye-cran40/' && \
    apt -y update && \
    apt -y install r-base

# Install quarto 
RUN curl -LO https://github.com/quarto-dev/quarto-cli/releases/download/v1.3.302/quarto-1.3.302-linux-amd64.deb && \
    dpkg -i quarto-1.3.302-linux-amd64.deb && \
    apt install -f && \
    rm -f quarto-1.3.302-linux-amd64.deb && \
    quarto install tinytex
